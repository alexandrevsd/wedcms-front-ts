import ColumnContainer from '../layout/main/ColumnContainer';
import LeftColumn from '../layout/main/LeftColumn';
import BoxNews from '../layout/main/boxs/BoxNews';
import RightColumn from '../layout/main/RightColumn';
import BoxPartner from '../layout/main/boxs/BoxPartner';
import InnerColumnsContainer from '../layout/main/InnerColumnsContainer';
import Column from '../layout/main/Column';
import { ColumnType } from '../../interfaces/ColumnProps';
import Box from '../layout/main/boxs/Box';
import { BoxType } from '../../interfaces/BoxProps';
import BoxAd from '../layout/main/boxs/BoxAd';
import { AdType } from '../../interfaces/AdProps';

export default function Home() {
  return (
    <ColumnContainer>
      <LeftColumn>
        <BoxNews />
        <InnerColumnsContainer>
          <Column type={ColumnType.FIRST}>
            <Box type={BoxType.ORANGE} title="Box title">
              test
            </Box>
            <Box type={BoxType.ORANGE} title="Box title">
              test
            </Box>
          </Column>
          <Column type={ColumnType.SECOND}>
            <Box type={BoxType.GREEN} title="Box title">
              test
            </Box>
            <Box type={BoxType.GREEN} title="Box title">
              test
            </Box>
          </Column>
          <Column type={ColumnType.THIRD}>
            <BoxAd
              title={'Publicité'}
              src={'./images/main/ads/2.png'}
              href={'http://wedcms.ddns.net/'}
              type={AdType.MIDDLE}
            />
            <Box type={BoxType.BLUE} title="Box title">
              test
            </Box>
          </Column>
        </InnerColumnsContainer>
      </LeftColumn>
      <RightColumn>
        <BoxPartner />
        <BoxAd
          title={'Publicité'}
          src={'./images/main/ads/3.png'}
          href={'http://wedcms.ddns.net/'}
          type={AdType.SIDE}
        />
      </RightColumn>
    </ColumnContainer>
  );
}
