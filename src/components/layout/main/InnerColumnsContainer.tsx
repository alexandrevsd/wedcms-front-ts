import IPropsChildren from '../../../interfaces/PropsChildren';

export default function InnerColumnsContainer({ children }: IPropsChildren) {
  return <div className="inner-columns-container">{children}</div>;
}
