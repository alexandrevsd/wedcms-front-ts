import ITabButtonProps from '../../../interfaces/TabButtonProps';

export default function TabButton({
  children,
  id,
  selected,
  setSelected,
  onTabOver,
  onTabOut,
}: ITabButtonProps) {
  const isActive = selected === id;
  const activeClass = isActive ? ' active' : '';
  const className = 'tab' + activeClass;
  return (
    <div
      onMouseEnter={onTabOver}
      onMouseLeave={onTabOut}
      onMouseOver={() => setSelected(id)}
      className={className}
    >
      <div className="tab-left"></div>
      <div className="tab-middle">
        <span className={id}>{children}</span>
      </div>
      <div className="tab-right"></div>
    </div>
  );
}
