import { useEffect, useState } from 'react';
import IHeaderProps from '../../../interfaces/Header';
import HeaderMobile from './HeaderMobile';
import HeaderNormal from './HeaderNormal';

export default function Header({ image }: IHeaderProps) {
  const [selectedTab, setSelectedTab] = useState<string>('my-habbo');
  const [tabHover, setTabHover] = useState<boolean>(false);
  const [tabTimeout, setTabTimeout] = useState<NodeJS.Timeout>();

  useEffect(() => {
    if (tabHover) {
      if (tabTimeout) {
        clearTimeout(tabTimeout);
        setTabTimeout(undefined);
      }
    } else {
      if (selectedTab !== 'my-habbo') {
        if (tabTimeout) {
          clearTimeout(tabTimeout);
          setTabTimeout(undefined);
        }
        setTabTimeout(setTimeout(() => setSelectedTab('my-habbo'), 3000));
      }
    }
  }, [tabHover]);

  const onTabOver = () => setTabHover(true);
  const onTabOut = () => setTabHover(false);

  return (
    <>
      <HeaderMobile
        setSelectedTab={setSelectedTab}
        selectedTab={selectedTab}
        onTabOut={onTabOut}
        onTabOver={onTabOver}
        bannerSrc={'./images/main/mobile/header/banners/' + image + '.png'}
      />
      <HeaderNormal
        setSelectedTab={setSelectedTab}
        selectedTab={selectedTab}
        onTabOut={onTabOut}
        onTabOver={onTabOver}
        bannerSrc={'./images/main/header/banners/' + image + '.png'}
      />
    </>
  );
}
