// TODO: Essayer de rendre plus propre et rendre les habbos connectés et le partie "non connecté" configurable

import IHeaderChildProps from '../../../interfaces/HeaderChildProps';
import Menu from './Menu';
import TabsButtons from './TabsButtons';
import TabsContents from './TabsContents';

export default function HeaderMobile({
  selectedTab,
  setSelectedTab,
  onTabOver,
  onTabOut,
  bannerSrc,
}: IHeaderChildProps) {
  return (
    <header className="screen-mobile">
      <div className="logo">
        <a href="/">
          <img src="./images/main/header/logo.png" />
        </a>
      </div>
      <div className="topbar-container">
        <div className="topbar-left"></div>
        <div className="topbar-middle">
          <div className="stripe-container">
            <div className="stripe-left"></div>
            <div className="stripe-middle">0 Habbos connectés</div>
            <div className="stripe-right"></div>
          </div>
          <div className="stripe-container">
            <div className="stripe-left"></div>
            <div className="stripe-middle-right stripe-middle red">
              Non connecté
            </div>
            <div className="stripe-right"></div>
          </div>
          <TabsButtons
            setSelectedTab={setSelectedTab}
            selectedTab={selectedTab}
            onTabOver={onTabOver}
            onTabOut={onTabOut}
          />
        </div>
        <div className="topbar-right"></div>
      </div>
      <div
        className="banner"
        style={{
          backgroundImage: "url('" + bannerSrc + "')",
          backgroundPosition: '0 -4px',
        }}
      >
        <div className="banner-opacity">
          <div className="banner-container">
            <TabsContents
              selectedTab={selectedTab}
              onTabOver={onTabOver}
              onTabOut={onTabOut}
            />
            <Menu mobile />
          </div>
          <div className="opacity"></div>
        </div>
      </div>
      <div className="submenu"></div>
      <div className="submenu_bottom"></div>
    </header>
  );
}
