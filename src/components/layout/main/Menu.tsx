import IMenuProps from '../../../interfaces/MenuProps';
import MenuLink from './MenuLink';

export default function Menu({ mobile }: IMenuProps) {
  return (
    <div className="menu">
      <ul>
        <MenuLink to="/" icon="home">
          {!mobile && 'Accueil'}
        </MenuLink>
        <MenuLink to="/hotel" icon="hotel">
          {!mobile && 'Nouveau?'}
        </MenuLink>
        <MenuLink to="/community" icon="community">
          {!mobile && 'Communauté'}
        </MenuLink>
        <MenuLink to="/fun" icon="fun">
          {!mobile && 'Evénement'}
        </MenuLink>
        <MenuLink to="/games" icon="games">
          {!mobile && 'Jeux'}
        </MenuLink>
        <MenuLink to="/mobile" icon="mobile">
          {!mobile && 'Portable'}
        </MenuLink>
        <MenuLink to="/shop" icon="shop">
          {!mobile && 'Shop'}
        </MenuLink>
        <MenuLink to="/help" icon="help">
          {!mobile && 'Aide&Sécu'}
        </MenuLink>
        <MenuLink to="/coins" icon="coins" last>
          {!mobile && 'Crédits'}
        </MenuLink>
      </ul>
      {!mobile && <div className="submenu"></div>}
    </div>
  );
}
