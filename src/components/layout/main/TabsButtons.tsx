import ITabsButtonsProps from '../../../interfaces/TabsButtonsProps';
import TabButton from './TabButton';

export default function TabsButtons({
  selectedTab,
  setSelectedTab,
  onTabOut,
  onTabOver,
}: ITabsButtonsProps) {
  return (
    <div className="tabs-container">
      <TabButton
        id="my-habbo"
        selected={selectedTab}
        setSelected={setSelectedTab}
        onTabOver={onTabOver}
        onTabOut={onTabOut}
      >
        Mon Habbo
      </TabButton>
      <TabButton
        id="my-credits"
        selected={selectedTab}
        setSelected={setSelectedTab}
        onTabOver={onTabOver}
        onTabOut={onTabOut}
      >
        Mes crédits
      </TabButton>
      <TabButton
        id="habbo-club"
        selected={selectedTab}
        setSelected={setSelectedTab}
        onTabOver={onTabOver}
        onTabOut={onTabOut}
      >
        Habbo Club
      </TabButton>
    </div>
  );
}
