// TODO: Essayer de rendre plus propre et rendre les habbos connectés et le partie "non connecté" configurable

import IHeaderChildProps from '../../../interfaces/HeaderChildProps';
import Menu from './Menu';
import TabsButtons from './TabsButtons';
import TabsContents from './TabsContents';

export default function HeaderNormal({
  selectedTab,
  setSelectedTab,
  onTabOver,
  onTabOut,
  bannerSrc,
}: IHeaderChildProps) {
  return (
    <header className="screen-normal">
      <div className="topbar-container">
        <div className="topbar-left"></div>
        <div className="topbar-middle">
          <div className="stripe-container">
            <div className="stripe-left"></div>
            <div className="stripe-middle">0 Habbos connectés</div>
            <div className="stripe-right"></div>
          </div>
          <TabsButtons
            selectedTab={selectedTab}
            setSelectedTab={setSelectedTab}
            onTabOver={onTabOver}
            onTabOut={onTabOut}
          />
          <div className="stripe-container">
            <div className="stripe-left"></div>
            <div className="stripe-middle-right stripe-middle">
              <span className="red">Non connecté</span>
            </div>
            <div className="stripe-right"></div>
          </div>
        </div>
        <div className="topbar-right"></div>
      </div>
      <div
        className="banner"
        style={{
          backgroundImage: "url('" + bannerSrc + "')",
          backgroundPosition: '0 -4px',
        }}
      >
        <div className="banner-opacity">
          <div className="banner-container">
            <div>
              <a href="/" className="logo">
                <img src="./images/main/header/logo.png" alt="Logo" />
              </a>
            </div>
            <TabsContents
              selectedTab={selectedTab}
              onTabOver={onTabOver}
              onTabOut={onTabOut}
            />
            <a href="" className="enter">
              <img
                src="./images/main/header/enterHH/fr.gif"
                alt="Enter Habbo Hotel"
              />
            </a>
          </div>
          <Menu />
        </div>
        <div className="opacity"></div>
      </div>
    </header>
  );
}
