import IPropsChildren from '../../../interfaces/PropsChildren';

export default function ColumnContainer({ children }: IPropsChildren) {
  return <div className="columns-container">{children}</div>;
}
