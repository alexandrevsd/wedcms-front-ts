import { BoxType } from '../../../interfaces/BoxProps';
import IPropsChildren from '../../../interfaces/PropsChildren';
import BottomMenu from './BottomMenu';
import Box from './boxs/Box';

export default function Content({ children }: IPropsChildren) {
  return (
    <div className="content">
      {children}
      <Box type={BoxType.NO_H_BLUE} copyright>
        <p className="copyright-menu">
          <BottomMenu />
        </p>
        <p>© Copyright WedCMS, powered by WedCMS</p>
      </Box>
    </div>
  );
}
