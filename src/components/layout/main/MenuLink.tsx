import { Link } from 'react-router-dom';
import { useLocation } from 'react-router';
import IMenuLinkProps from '../../../interfaces/MenuLinkProps';

export default function MenuLink({ to, last, icon, children }: IMenuLinkProps) {
  const location = useLocation();
  const isActive = to === location.pathname ? ' active' : '';
  const className = last ? 'last' + isActive : '' + isActive;
  return (
    <li className={className}>
      <span className="left"></span>
      <Link to={to}>
        <img src={`./images/main/header/nav/${icon}.png`} alt="" /> {children}
      </Link>
      <span className="right"></span>
    </li>
  );
}
