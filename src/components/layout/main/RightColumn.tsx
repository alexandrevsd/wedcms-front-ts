import IPropsChildren from '../../../interfaces/PropsChildren';

export default function RightColumn({ children }: IPropsChildren) {
  return <div className="column-right">{children}</div>;
}
