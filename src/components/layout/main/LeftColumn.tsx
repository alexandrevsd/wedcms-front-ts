import IPropsChildren from '../../../interfaces/PropsChildren';

export default function LeftColumn({ children }: IPropsChildren) {
  return <div className="column-left">{children}</div>;
}
