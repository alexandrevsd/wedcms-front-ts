import { IColumnProps } from '../../../interfaces/ColumnProps';

export default function Column({ children, type }: IColumnProps) {
  return <div className={type + '-column'}>{children}</div>;
}
