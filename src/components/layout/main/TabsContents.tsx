import ITabsContentsProps from '../../../interfaces/TabsContentsProps';
import HabboClub from './tabs/HabboClub';
import MyCredits from './tabs/MyCredits';
import MyHabbo from './tabs/MyHabbo';

export default function TabsContents({
  selectedTab,
  onTabOver,
  onTabOut,
}: ITabsContentsProps) {
  return (
    <div
      className="tabs-contents-container"
      onMouseEnter={onTabOver}
      onMouseLeave={onTabOut}
    >
      <div className="tab-content">
        {selectedTab === 'my-habbo' && <MyHabbo />}
        {selectedTab === 'my-credits' && <MyCredits />}
        {selectedTab === 'habbo-club' && <HabboClub />}
        <div className="clear"></div>
      </div>
      <div className="tab-bottom"></div>
    </div>
  );
}
