export default function BoxNews() {
  return (
    <div className="announcements-container">
      <div className="news-left">
        <div className="news-left-top"></div>
        <div className="news-left-content">
          <div className="news-left-top-content">
            <span className="active">1</span>
            <span>2</span>
            <span>3</span>
            <span>4</span>
            <span>5</span>
          </div>
          <img
            src="./images/main/announcements/02_battlefield_newLogo.gif"
            className="announcement-image"
          />
          <div className="news-left-separator"></div>
          <div className="news-left-bottom-content">
            <p>Toutes les infos de la communauté Habbo</p>
            <div className="announcement-buttons">
              <a href="/" className="btn-blue">
                C'est ici !
              </a>
            </div>
          </div>
        </div>
        <div className="news-left-bottom"></div>
      </div>
      <div className="news-right">
        <div className="news-right-top"></div>
        <div className="news-right-content">
          <div className="news-right-top-content">
            Quoi de neuf?
            <a href="" className="news-rss">
              <img src="./images/main/rss.png" />
            </a>
          </div>
          <div className="news-right-item">
            <span>
              [08-03-22] <a href="">Ceci est un essai</a>
            </span>
            <span>Description plus détaillée ici...</span>
          </div>
          <div className="news-right-item">
            <span>
              [08-03-22] <a href="">Ceci est un essai</a>
            </span>
            <span>Description plus détaillée ici...</span>
          </div>
          <div className="news-right-separator"></div>
          <div className="news-right-item">
            <span>
              [08-03-22] <a href="">Ceci est un essai</a>
            </span>
          </div>
          <div className="news-right-item">
            <span>
              [08-03-22] <a href="">Ceci est un essai</a>
            </span>
          </div>
          <a href="/" className="btn-blue">
            Plus d'infos
          </a>
        </div>
        <div className="news-right-bottom"></div>
      </div>
    </div>
  );
}
