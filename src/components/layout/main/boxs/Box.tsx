import { BoxType, IBoxProps } from '../../../../interfaces/BoxProps';

export default function Box({ children, type, title, copyright }: IBoxProps) {
  let className = 'box box-noh-blue';
  if (copyright) {
    className += ' copyright';
  }
  return type !== BoxType.NO_H_BLUE ? (
    <div className={'box box-' + type}>
      <div className="box-top">
        <div className="box-top-top">
          <div className="box-top-corner-left"></div>
          <div className="box-top-corner-middle"></div>
          <div className="box-top-corner-right"></div>
        </div>
        <div className="box-top-middle">{title}</div>
        <div className="box-top-bottom">
          <div className="box-bottom-corner-left"></div>
          <div className="box-bottom-corner-middle"></div>
          <div className="box-bottom-corner-right"></div>
        </div>
      </div>
      <div className="box-content">{children}</div>
      <div className="box-bottom">
        <div className="box-bottom-left"></div>
        <div className="box-bottom-middle"></div>
        <div className="box-bottom-right"></div>
      </div>
    </div>
  ) : (
    <div className={className}>
      <div className="box-top">
        <div className="box-top-corner-left"></div>
        <div className="box-top-corner-middle"></div>
        <div className="box-top-corner-right"></div>
      </div>
      <div className="box-content">{children}</div>
      <div className="box-bottom">
        <div className="box-bottom-corner-left"></div>
        <div className="box-bottom-corner-middle"></div>
        <div className="box-bottom-corner-right"></div>
      </div>
    </div>
  );
}
