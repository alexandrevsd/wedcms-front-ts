import IAdProps from '../../../../interfaces/AdProps';

export default function BoxAd({ title, href, src, type }: IAdProps) {
  const classNameStart = 'box-supp-' + type;
  return (
    <div className={classNameStart}>
      <div className={classNameStart + '-top'}></div>
      <div className={classNameStart + '-middle'}>
        <div className={classNameStart + '-top-content'}>{title}</div>
        <a href={href} target="_blank">
          <img src={src} />
        </a>
      </div>
      <div className={classNameStart + '-bottom'}></div>
    </div>
  );
}
