import { Route, Routes } from 'react-router';
import Home from '../pages/Home';
import NotFound from '../pages/NotFound';
import Content from './main/Content';
import Header from './main/Header';
import BoxAd from './main/boxs/BoxAd';
import { AdType } from '../../interfaces/AdProps';

export default function Main() {
  return (
    <div className="container">
      <BoxAd
        title={'Publicité'}
        src={'./images/main/ads/1.png'}
        href={'http://wedcms.ddns.net/'}
        type={AdType.TOP}
      />
      <Header image="fr" />
      <Content>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Content>
      <div className="bottom"></div>
    </div>
  );
}
