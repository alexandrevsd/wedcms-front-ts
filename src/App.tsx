import Main from './components/layout/Main';
import './App.css';

function App() {
  document.body.style.backgroundImage =
    'url(/images/main/backgrounds/habbo.png)';

  return (
    <div className="App">
      <Main />
    </div>
  );
}

export default App;
