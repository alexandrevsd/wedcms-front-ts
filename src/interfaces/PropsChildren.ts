export default interface IPropsChildren {
  children: JSX.Element | JSX.Element[];
}
