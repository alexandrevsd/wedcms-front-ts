export enum AdType {
  TOP = 'top',
  SIDE = 'side',
  MIDDLE = 'middle',
}

export default interface IAdProps {
  title: string;
  src: string;
  href: string;
  type: AdType;
}
