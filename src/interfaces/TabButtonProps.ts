import { MouseEventHandler } from 'react';

export default interface ITabButtonProps {
  id: string;
  selected: string;
  children: string;
  setSelected: Function;
  onTabOver: MouseEventHandler;
  onTabOut: MouseEventHandler;
}
