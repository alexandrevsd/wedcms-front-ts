import { MouseEventHandler } from 'react';

export default interface ITabsButtonsProps {
  setSelectedTab: Function;
  selectedTab: string;
  onTabOver: MouseEventHandler;
  onTabOut: MouseEventHandler;
}
