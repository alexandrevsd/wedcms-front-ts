export enum BoxType {
  ORANGE = 'orange',
  GREEN = 'green',
  BLUE = 'blue',
  BROWN = 'brown',
  NO_H_BLUE = 'noh-blue',
}

export interface IBoxProps {
  type: BoxType;
  title?: string;
  copyright?: boolean;
  children: undefined | null | JSX.Element | JSX.Element[] | string;
}
