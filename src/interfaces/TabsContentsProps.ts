import { MouseEventHandler } from 'react';

export default interface ITabsContentsProps {
  selectedTab: string;
  onTabOver: MouseEventHandler;
  onTabOut: MouseEventHandler;
}
