import { MouseEventHandler } from 'react';

export default interface IHeaderChildProps {
  setSelectedTab: Function;
  selectedTab: string;
  onTabOver: MouseEventHandler;
  onTabOut: MouseEventHandler;
  bannerSrc: string;
}
