export default interface IMenuLinkProps {
  to: string;
  icon: string;
  last?: boolean;
  children: false | undefined | string | JSX.Element | JSX.Element[];
}
