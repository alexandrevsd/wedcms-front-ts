export enum ColumnType {
  FIRST = 'first',
  SECOND = 'second',
  THIRD = 'third',
}

export interface IColumnProps {
  type: ColumnType;
  children: JSX.Element | JSX.Element[];
}
